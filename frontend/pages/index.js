import React from 'react'
import Link from "next/link"
import { log } from 'util';
import fetch from 'isomorphic-unfetch'
import Questions from "../components/Questions"
import { connect } from 'react-redux'
import { bindActionCreators } from "redux"
import { dispatchEmotion } from "../store/emotion_reducer/action"


class Home extends React.Component {
    
    static async getInitialProps({ req }) {
        
        const res = await fetch("http://localhost:5000/api/emotions/getAll")
        const emotions_raw = await res.json()
        
        var emotions = []
        for (var k in emotions_raw) {
            emotions.push(emotions_raw[k])
        }

        return { emotions : emotions }
    }

    static propTypes () {
        return {
            emotion : React.PropTypes.object
        }
    }

    
    render() {
        return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-12 d-flex justify-content-center">
                        <h1> <i>How do you feel ? </i></h1>
                    </div>
                </div>
                <div>
                    <Questions emotions={this.props.emotions}/>
                </div>
            </div>
        </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
      dispatchEmotion: bindActionCreators(dispatchEmotion, dispatch),
    }
  }
  

export default connect(null, mapDispatchToProps)(Home);

