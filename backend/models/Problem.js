var mongoose = require('mongoose');

var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');


var ProblemSchema = new mongoose.Schema({
    problem_full: {type: String, unique: true, required: [true, "can't be blank"], index: true},
    emotions: [String],
    location: {
        type:{
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    general_importance: {type: Number, default: 0},
    user_importance: {type: Number, default: 0},
    
}, {timestamps: true})

mongoose.model("Problem", ProblemSchema)