var mongoose = require('mongoose');
var router = require('express').Router();
var Post = mongoose.model('Post');


router.get("/getAll", (req, res) => {
    Post.find({}, (err, post) => {
    var postMap = {};
    
    post.forEach(function(post) {
        postMap[post._id] = post;
    });

    res.send(postMap);  
    })

})

router.get("/getOne", (req, res) => {
    Post.findOne({}, (err, post) => {
    res.send(post);  
    })

})



router.post("/add", (req, res) => {
    
    var newPost = new Post();
    newPost.title = req.body.title
    newPost.postText = req.body.postText
    newPost.location.coordinates = req.body.coordinates
    newPost.location.type = "Point"

    newPost.save((err, post) => {
        if (err) {
            console.log(err);
            
            res.send("error")
        } else {
            console.log(post);
            res.send(post)      
        }
    })
})



module.exports = router;