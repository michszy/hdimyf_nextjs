import {createStore, applyMiddleware, combineReducers } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunkMiddleware from 'redux-thunk'
import emotion_reducer from "./emotion_reducer/reducer"
import problem_reducer from "./problem_reducer/reducer"


const bindMiddleware = middleware => {
    if (process.env.NODE_ENV !== 'production') {
      const { composeWithDevTools } = require('redux-devtools-extension')
      return composeWithDevTools(applyMiddleware(...middleware))
    }
    return applyMiddleware(...middleware)
  }

  export const initStore = () => {
    return createStore(
      combineReducers({
        emotion_reducer,
        problem_reducer,
      }),
      bindMiddleware([thunkMiddleware])
    )
  }