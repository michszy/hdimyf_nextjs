var mongoose = require('mongoose');

var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');


var EmotionSchema = new mongoose.Schema({
    emotion_name: {type: String, unique: true, required: [true, "can't be blank"], index: true},
    emotion_nickname: {type: String, unique: true, required:[true, "can't be blank"], index: true},
    color: {type:String, default: "#cc0044"},
    location: {
        type:{
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    general_importance: {type: Number, default: 0},
    user_importance: {type: Number, default: 0},
  
}, {timestamps: true})


mongoose.model("Emotion", EmotionSchema)


//user_id: [
//    {type: Schema.Types.ObjectId, ref: "User"}
//]