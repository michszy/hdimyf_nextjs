
const initialState = {
    problem_choosed : "undefined",
    problem_choosed_lat_long :[]
}


export default (state = initialState, action ) => {
    switch (action.type) {
        case "PROBLEM_CHOOSEN": 
            return {
                ...state,
                problem_choosed : action.problem_choosed,
                problem_choosed_lat_long: action.problem_choosed_lat_long
            }

        default:
            return state
    }
}
