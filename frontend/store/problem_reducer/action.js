export const ProblemtActionTypes = {
    PROBLEM_CHOOSEN: 'PROBLEM_CHOOSEN',
}

export const dispatchProblem = () => dispatch => {
    return dispatch({ type: ProblemtActionTypes.PROBLEM_CHOOSEN })
}