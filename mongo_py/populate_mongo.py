from pymongo import MongoClient
import json
import requests

def populate_emotions():
    with open("./emotions.json") as f:
        data = json.load(f)

    for doc in data["emotions_list"]:
        r = requests.post("http://localhost:5000/api/emotions/add", 
        {
            "emotion_name": doc["emotion_name"], 
            "emotion_nickname": doc["emotion_nickname"],
            "color": doc["color"],
            "coordinates": doc["coordinates"]
        }
        )

        print(r.text)


def populate_problems():
    with open("./problems.json") as f:
        data = json.load(f)

    for doc in data["problems_list"]:
        r = requests.post("http://localhost:5000/api/problems/add", 
        {
            "problem_full": doc["problem_full"], 
            "emotions": doc["emotions"],
            "coordinates": doc["coordinates"]
        }
        )

        print(r.text)

#populate_emotions()
populate_problems()

