var mongoose = require('mongoose');

var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');


var PostSchema = new mongoose.Schema({
    title: {type: String, unique: true, required: [true, "can't be blank"], index: true},
    postText: {type: String, unique: true, required:[true, "can't be blank"], index: true},
    location: {
        type:{
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    likes: {type: Number, default: 0},
    comments: {type: Array, default: 0},
  
}, {timestamps: true})


mongoose.model("Post", PostSchema)


//user_id: [
//    {type: Schema.Types.ObjectId, ref: "User"}
//]