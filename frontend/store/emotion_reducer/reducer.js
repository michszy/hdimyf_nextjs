

const initialState = {
    emotion_choosed : "undefined",
    emotion_choosed_lat_long :[]
}


export default (state = initialState, action ) => {
    switch (action.type) {
        case "EMOTION_CHOOSEN":
            return {
                ...state,
                emotion_choosed : action.emotion_choosed,
                emotion_choosed_lat_long : action.emotion_choosed_lat_long
            }
        default:
            return state
        }
    }