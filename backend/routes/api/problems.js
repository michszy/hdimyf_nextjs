var mongoose = require('mongoose');
var router = require('express').Router();

var Problem = mongoose.model('Problem');

var milesToRadian = (km) => {
    var earthRadiusInKm = 6371;
    return km / earthRadiusInKm
}

router.post("/findProblems", (req, res) => {
    
    
    var emotionCoordinates = req.body.emotionCoordinates
    var maxDistance = req.body.maxDistance
        Problem.find({
            location: {
                $geoWithin: { 
                    $centerSphere: [
                        emotionCoordinates, maxDistance / 6378.1
                    ]
                }
            }
        }, (err, locations ) => {
            if (err) {
                console.log(err);
                res.send(err)
            } else {
                res.send(locations)
            }
            
        })
})


router.get("/getAll", (req, res) => {
    Problem.find({}, (err, problems) => {
        var problemsMap = {};
        problems.forEach(function(problem) {
            problemsMap[problem._id] = problem;
        });
        res.send(problemsMap);  
    })
})


router.post("/add", (req, res) => {
    
    var newProblem = new Problem();
    newProblem.problem_full = req.body.problem_full
    newProblem.emotions = req.body.emotions
    newProblem.location.coordinates = req.body.coordinates
    newProblem.location.type = "Point"

    newProblem.save((err, problem) => {
        if (err) {
            console.log(err);
            res.send("error")
        } else {
            console.log(problem);
            res.send(problem)
            
        }
    })
  
})


module.exports = router;