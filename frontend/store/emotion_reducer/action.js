export const EmotiontActionTypes = {
    EMOTION_CHOOSEN: 'EMOTION_CHOOSEN',
  }
  
  export const dispatchEmotion = () => dispatch => {
    return dispatch({ type: EmotiontActionTypes.EMOTION_CHOOSEN })
  }