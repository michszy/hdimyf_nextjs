var router = require('express').Router();

router.use('/emotions', require('./emotions'));
router.use("/problems", require("./problems"))


module.exports = router;