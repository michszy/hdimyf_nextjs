import Head from 'next/head'

const Meta = () => (
    <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
        {/*<link rel="shortcut icon" href="/static/favicon.png" />*/}
        {/*<link rel="stylesheet" type="text/css" href="/static/nprogress.css" />*/}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
        <title>How does it makes you feel ?</title>
    </Head>
    
)

export default Meta