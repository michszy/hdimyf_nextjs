import React from "react";

const Books = () => {
    return (
        <div>
            <iframe width="100%" height="500px"
                    src="https://www.senscritique.com/livre/Les_Souffrances_du_jeune_Werther/105929"></iframe>
            <div className="right">
                <button className="btn btn-success btn-lg"> Another One</button>
                <br/>
                <br/>
                <button type="button" className="btn btn-info btn-lg">Post One</button>
            </div>
        </div>
    )
}

export default Books