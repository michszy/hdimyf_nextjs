import App, { Container } from "next/app"
import Page from "../components/Pages"
import { Provider } from "react-redux"
import  { initStore } from "../store/store"
import withRedux from 'next-redux-wrapper'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "../components/Navbar"

class myApp extends App {
    render() {
        const { Component, pageProps, store } = this.props
        return (
            <Provider store={store}>
                    <Navbar/>
                    <Component {...pageProps}/>
            </Provider>
        )
    }
}


export default withRedux(initStore)(myApp);