import Navbar from "./Navbar"
import styled from "styled-components"
import Link from "next/link"
import Router from "next/router";
import NProgress from "nprogress";



Router.onRouteChangeStart = () => {
    console.log("Start");

    NProgress.start();   
}

Router.onRouteChangeComplete = () => {
    console.log("Change");
    NProgress.done()
    
}

Router.onRouteChangeError = () => {
    console.log("Error");
    NProgress.done()
    
}

const Header = () =>  (
<div><h1>Header</h1></div>
)


export default Header;