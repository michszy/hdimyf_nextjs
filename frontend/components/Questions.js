import React from "react"
import { cpus } from "os";
import { log } from "util";
import Link from 'next/link'
import { useSelector, useDispatch } from "react-redux"



const dispatchOnComponentQuestion = () => {
    const dispatch = useDispatch()

    const dispatchEmotion = (emotion_nickname, location) => {
        dispatch({
                type: "EMOTION_CHOOSEN",
                emotion_choosed :emotion_nickname,
                emotion_choosed_lat_long : location
            })
        }
    return { dispatchEmotion }
}



const Questions = (props) => {
   
    const emotions = props.emotions

    const { dispatchEmotion } = dispatchOnComponentQuestion()
    return(
    <div>
        {emotions.map((value, index) => {
            return (
                    <div>
                    <Link key={`link${emotions[index]._id}`} href="/problems">
                        <button key={`button${emotions[index]._id}`} onClick={ (e) => dispatchEmotion(emotions[index].emotion_nickname, emotions[index].location)}>
                            {emotions[index].emotion_name} 
                        </button>
                    </Link>
                    </div>
                    )
        })
    }
    </div>
        )
    }


export default Questions;