#import matplotlib.pyplot as plt
import json
import numpy as np
import itertools
import folium
from IPython.display import display

with open("./emotions.json") as f:
    emotion_map = json.load(f)

with open('./problems.json') as f:
    problems_map = json.load(f)

lat_loc = []
name = []
count_emo = 0
count_pro = 0




m = folium.Map(location=[48.137154, 11.576124], zoom_start=4,tiles='Stamen Toner')

for doc in emotion_map["emotions_list"]:
    if "coordinates" in doc:
        folium.Marker(
            doc["coordinates"],
            popup=doc["emotion_name"]
        ).add_to(m)

for doc in problems_map["problems_list"]:
    if "coordinates" in doc:
        folium.Circle(
            location=doc["coordinates"],
            radius= 900000,
            popup=doc["problem_full"]
        ).add_to(m)

m.save("map.html")

quit()

for doc in emotion_map["emotions_list"]:
    if "geoloc" in doc:
        lat_loc.append(doc["geoloc"])
        name.append(doc["emotion_name"])
        count_emo += 1


for doc in problems_map["problems_list"]:
    if "geoloc" in doc:
        lat_loc.append(doc["geoloc"])
        name.append(doc["problem_full"])
        count_pro += 1

x = [ x[1] for x in lat_loc]
y = [ y[0] for y in lat_loc]


c = []
[c.append(0) for x in range(count_emo)]
[c.append(5) for x in range(count_pro)]

cmap = plt.cm.RdYlGn
norm = plt.Normalize(1,4)

fig,ax = plt.subplots()

# https://stackoverflow.com/questions/53630158/add-points-to-the-existing-matplotlib-scatter-plot

sc = plt.scatter(x,y ,c=c, s=100, cmap=cmap, norm=norm)


print(sc)
annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
annot.set_visible(False)

def update_annot(ind):

    pos = sc.get_offsets()[ind["ind"][0]]
    annot.xy = pos
    text = "{}, {}".format(" ".join(list(map(str,ind["ind"]))), 
                           " ".join([name[n] for n in ind["ind"]]))
    annot.set_text(text)
    annot.get_bbox_patch().set_facecolor(cmap(norm(c[ind["ind"][0]])))
    annot.get_bbox_patch().set_alpha(0.4)


def hover(event):
    vis = annot.get_visible()
    if event.inaxes == ax:
        cont, ind = sc.contains(event)
        if cont:
            update_annot(ind)
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            if vis:
                annot.set_visible(False)
                fig.canvas.draw_idle()

fig.canvas.mpl_connect("motion_notify_event", hover)

plt.show()