var mongoose = require('mongoose');
var router = require('express').Router();
var Emotion = mongoose.model('Emotion');


router.get("/getAll", (req, res) => {
    Emotion.find({}, (err, emotions) => {
    var emotionsMap = {};
    
    emotions.forEach(function(emotion) {
        emotionsMap[emotion._id] = emotion;
    });

    res.send(emotionsMap);  
    })

})

router.post("/add", (req, res) => {
    
    var newEmotion = new Emotion();
    newEmotion.problem_full = req.body.problem_full
    newEmotion.color = req.body.color
    newEmotion.location.coordinates = req.body.coordinates
    newEmotion.location.type = "Point"

    newEmotion.save((err, emotion) => {
        if (err) {
            console.log(err);
            
            res.send("error")
        } else {
            console.log(emotion);
            res.send(emotion)
            
        }
    })

    
})



module.exports = router;