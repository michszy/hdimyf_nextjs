import React from 'react'
import {Container, Row, Col, Nav, Tab } from "react-bootstrap"
import Articles from "../components/Solutions/Articles"
import Books from "../components/Solutions/Books"
import Memes from "../components/Solutions/Memes"
import Merchs from "../components/Solutions/Merchs"
import Musics from "../components/Solutions/Musics"
import Podcasts from "../components/Solutions/Podcasts"
import Posts from "../components/Solutions/Posts"
import Videos from "../components/Solutions/Videos"


class Solutions extends React.Component {
    // replace by react bootstrap
    constructor(props) {
        super(props)
      }
    render() {
        return (
            <Container>
            <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                <Row>
                    <Col sm={3}>    
                    <Nav variant="pills" className="flex-column">
                    <Nav.Item>
                        <Nav.Link eventKey="posts">Posts</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="articles">Articles</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="video">Videos</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="memes">Memes</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="podcasts">Podcasts</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="music">Musics</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="books">Books</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="merch">Merchs</Nav.Link>
                    </Nav.Item>

                    </Nav>
                    </Col>
                    <Col sm={9}>
                    <Tab.Content>
                        <Tab.Pane eventKey="posts">
                                <Posts />
                        </Tab.Pane>
                        <Tab.Pane eventKey="articles">
                                <Articles />
                        </Tab.Pane>
                        <Tab.Pane eventKey="video">
                                <Videos />
                        </Tab.Pane>
                        <Tab.Pane eventKey="memes">
                                <Memes />
                        </Tab.Pane>
                        <Tab.Pane eventKey="podcasts">
                            <Podcasts />
                        </Tab.Pane>
                        <Tab.Pane eventKey="music">
                            <Musics />
                        </Tab.Pane>
                        <Tab.Pane eventKey="books">
                            <Books />
                        </Tab.Pane>
                        <Tab.Pane eventKey="merch">
                            <Merchs />
                        </Tab.Pane>
                    </Tab.Content>
                    </Col>
                </Row>
                </Tab.Container>
                </Container>
        )
    }
}

export default Solutions