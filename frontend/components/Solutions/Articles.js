import React from "react";

const Articles = () => {
    return (
    <div>
        <iframe width="100%" height="500px" src="https://psychologia.co/why-am-i-so-sad/" allow="fullscreen"></iframe>
        <br/>
        <div className="right">
            <button className="btn btn-success btn-lg"> Another One </button>
            <br/>
            <br/>
            <button  type="button" className="btn btn-info btn-lg">Post One</button>
        </div>
    </div>
    )
}

export default Articles