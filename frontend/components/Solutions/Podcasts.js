import React from "react";

const Podcasts = () => {
    return (
        <div>
        <iframe width="100%" height="500px" src="https://www.youtube.com/embed/-SJywvgaJEI" frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen/>
        <br/>
        <div className = "right">
        <button className = "btn btn-success btn-lg"> Another One </button>
        <br/>
        <br/>
    <button type = "button" className = "btn btn-info btn-lg"> Post One </button>
        </div>
        </div>
    )
}

export default Podcasts