

const Posts = () => {
    return (
        <div>
            <h4>Definitely been there too my friend. What has worked really well for me is writing about what the healthier alternative to drinking/drugging would be and then remembering to read what I wrote over and over again until I can start making it happen. Therapy was also very important for me because I was using alcohol and Netflix as an escape from responsibility and I wouldn’t have learned that unless I had help from others who could see my behaviors.</h4>
            <h4>I’m in the same boat. I’ll keep watching for someone to comment a cure...but I’m thirty and have been depressed since I was probably about 9 or 10. Two unsuccessful attempts one when I was in my young teens and one fairly recently, my family knows about both but still when I say I need help or just a shoulder I have to come to a reddit page and cry to strangers who probably aren’t actually as concerned as they say.</h4>
            <h4>But what exactly do you think makes it fade away? I also have dissociation disorder but I feel like the more I let it develop, the more it irreversibily becomes my true self. After my first couple of years with depression, I started casting this way of life, identifying every unfulfilled need that came after losing contact with people around me, experimenting with self-dependent ways to replace them until I could actually go through large periods of time with no human contact whatsoever. But the sorrow at the moments of realisation and the anxiety from losing my social senses grew larger. At one point I decided to make every choice, as hard as it felt, just to snap out of it. Another 3-4 years spent working to get out of emotional isolation and live like a normal human being, with zero accomplishments except professionally and then I reached my limit again. At that point my brain reverted back to the dissociation state, trying to save me from a tragic outcome. I just don't know if I should oppose it any more, since it might be the healthiest option considering the alternatives.</h4>
            <br/>
            <p>Source : Reddit r/depression</p>
            <button className="btn btn-success btn-lg"> Another One </button>
            <br/>
            <br/>
            <button  type="button" className="btn btn-info btn-lg">Post One</button>
    </div>
    )
}
export default Posts