import React from 'react'
import Router, {withRouter, useRouter } from 'next/router'
import Link from "next/link"
import { log } from 'util';
import { connect, useSelector, useDispatch } from 'react-redux'
import Problems from "../components/Problems"
import { dispatchProblem } from "../store/problem_reducer/action";
import {bindActionCreators} from "redux";
import {dispatchEmotion} from "../store/emotion_reducer/action";


class problems extends React.Component {
    constructor(props) {
        super(props)
    }

    static getInitialProps = async ({ store, isServer }) => {

        const res = await fetch(
            "http://localhost:5000/api/problems/findProblems", {
            method : "POST",
            headers:{
                'Content-Type': 'application/json' 
                }, 
            body: JSON.stringify({
                emotionCoordinates: store.getState().emotion_reducer.emotion_choosed_lat_long.coordinates,
                maxDistance: 1000
            })
            }
        )

        const callAgain = (maxDistance) => {
            const res =  fetch(
                "http://localhost:5000/api/problems/findProblems", {
                method : "POST",
                headers:{
                    'Content-Type': 'application/json' 
                    }, 
                body: JSON.stringify({
                    emotionCoordinates: store.getState().emotion_reducer.emotion_choosed_lat_long.coordinates,
                    maxDistance: maxDistance
                })
                }
            ).then((res) => {
                return res.json()
            })
        }

        const problems_raw = await res.json()
        /*
        if (problems_raw.length < 3) {
            problems_raw = callAgain(10000)
    
        }
        */
       
        var problems = []
        for (var k in problems_raw) {
            problems.push(problems_raw[k])
        }
        
        return { isServer,  problems } 
        
    }

    render() {

    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-12 d-flex justify-content-center">
                        <h1> <i>How do you feel ? </i></h1>
                    </div>
                </div>
                <div>
                    <Problems problems={this.props.problems} />
                </div>
            </div>
        </div>
    )
}
}

const mapStateToProps = state => ({
    emotion_choosed : state.emotion_choosed,
    emotion_choosed_lat_long : state.emotion_choosed_lat_long
})


const mapDispatchToProps = dispatch => {
    return {
        dispatchProblem : bindActionCreators(dispatchProblem, dispatch)
    }
  }


// https://stackoverflow.com/questions/59114313/how-to-access-router-params-in-getinitialprops-stateles-component-in-nextjs
export default connect(mapStateToProps, mapDispatchToProps)(problems);